import os
BASE_DIR = os.path.dirname(os.path.abspath(__file__))
import torch
import random
from torch.utils.data import DataLoader
import torchvision.transforms as transforms
from PIL import Image
from matplotlib import pyplot as plt
path_lenet = os.path.abspath(os.path.join(BASE_DIR, 'model', 'lenet.py'))
path_tools = os.path.abspath(os.path.join(BASE_DIR, 'tools', 'common_tools.py'))
assert os.path.exists(path_lenet), "{}不存在，请将lenet.py文件放到{}".format(path_lenet, os.path.dirname(path_lenet))
assert os.path.exists(path_tools), "{}不存在，请将common_tools.py文件放到{}".format(path_tools, os.path.dirname(path_tools))


import sys
hello_pytorch_DIR = os.path.abspath(os.path.abspath(os.path.dirname(__file__) + os.path.sep + '..'))
sys.path.append(hello_pytorch_DIR)

from deepeye.tools.my_dataset import RMBDataset
from deepeye.tools.common_tools import set_seed, transform_invert

set_seed(1)   # 设置随机种子

# 参数设置
MAX_EPOCH = 10
BATCH_SIZE = 1
LR = 0.01
log_interval = 10
val_interval = 1
rmb_label = {'1': 0, '100':1}

# ============================ step 1/5 生成数据 ============================
split_dir = os.path.abspath(os.path.join(BASE_DIR, 'data', 'rmb_split'))
if not os.path.exists(split_dir):
    raise Exception('\n{} 不存在，回到06-dataloader_splitDataset.py生成数据。'.format(split_dir))
train_dir = os.path.join(split_dir, 'train')
valid_dir = os.path.join(split_dir, 'valid')

norm_mean = [0.485, 0.456, 0.406]
norm_std = [0.229, 0.224, 0.225]

train_transform = transforms.Compose([
    transforms.Resize((224, 224)),

    # 1. CenterCrop
    # transforms.CenterCrop(196),   # 若为512，大于224，则用黑色填充

    # 2. RandomCrop
    # transforms.RandomCrop(224, padding=16),
    # transforms.RandomCrop(224, padding=(16, 64)),
    # transforms.RandomCrop(224, padding=16, fill=(225, 0, 0)),
    # transforms.RandomCrop(512, pad_if_needed=True),
    # transforms.RandomCrop(224, padding=64, padding_mode='edge'),
    # transforms.RandomCrop(224, padding_mode='reflect'),
    # transforms.RandomCrop(1024, padding=1024, padding_mode='symmetric'),

    # 3. RandomResizedCrop
    # transforms.RandomResizedCrop(size=224, scale=(0.5, 0.5)),

    # 4. FiveCrop
    # transforms.FiveCrop(112),
    # # 对FiveCrop的输出进行变换，使之变为可执行的数据格式
    # # crops是FiveCrop输出的一个type，通过Lambda将 type转为张量的形式
    # transforms.Lambda(lambda crops: torch.stack([(transforms.ToTensor()(crop)) for crop in crops])),
    # 图片的维度变成了5维，换个方式可视化

    # 5. TenCrop
    # transforms.TenCrop(112, vertical_flip=False),
    # transforms.Lambda(lambda crops: torch.stack([(transforms.ToTensor()(crop)) for crop in crops])),

    # 1 Horizontal Flip
    # transforms.RandomHorizontalFlip(p=1),

    # 2 Vertical Flip
    # transforms.RandomVerticalFlip(p=0.5),

    # 3 RandomRotation
    # transforms.RandomRotation(90),
    # transforms.RandomRotation((90), expand=True),
    # transforms.RandomRotation(30, center=(0, 0)),     # 左上角旋转
    # transforms.RandomRotation(30, center=(0, 0), expand=True), # expand only for center rotation


    transforms.ToTensor(),
    transforms.Normalize(norm_mean, norm_std),
])

valid_transform = transforms.Compose([
    transforms.Resize((224, 224)),
    transforms.ToTensor(),
    transforms.Normalize(norm_mean, norm_std),
])


# 构建MyDataset实例
train_data = RMBDataset(data_dir=train_dir, transform=train_transform)
valid_data = RMBDataset(data_dir=valid_dir, transform=valid_transform)

# 构建DataLoader
train_loader = DataLoader(dataset=train_data, batch_size=BATCH_SIZE, shuffle=True)
valid_loader = DataLoader(dataset=valid_data, batch_size=BATCH_SIZE)

# ============================ step 5/5 训练 ============================
for epoch in range(MAX_EPOCH):
    for i, data in enumerate(train_loader):
        inputs, label = data  # B C H W

        img_tensor = inputs[0, ...]  # C H W
        # transform_invert对transform 进行逆操作，使得可观察到输入数据模型长什么样
        img = transform_invert(img_tensor, train_transform)
        plt.imshow(img)
        plt.show()
        plt.pause(0.5)
        plt.close()

        # bs, ncrops, c, h, w = inputs.shape   # 5维
        # for n in range(ncrops):
        #     img_tensor = inputs[0, n, ...]  # C H W
        #     img = transform_invert(img_tensor, train_transform)
        #     plt.imshow(img)
        #     plt.show()
        #     plt.pause(1)
