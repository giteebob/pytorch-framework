import torch
import torch.nn as nn
import sys, os
hello_pytorch_DIR = os.path.abspath(os.path.dirname(__file__)+os.path.sep+'..')
sys.path.append(hello_pytorch_DIR)

class Net(nn.Module):
    def __init__(self, neural_num, d_drop=0.5):
        super(Net, self).__init__()

        self.linears = nn.Sequential(
            nn.Dropout(d_drop),
            nn.Linear(neural_num, 1, bias=False),
            nn.ReLU(inplace=True)
        )

    def forward(self, x):
        return self.linears(x)

input_num = 10000
x = torch.ones((input_num, ), dtype=torch.float32)

net = Net(input_num, d_drop=0.5)
net.linears[1].weight.detach().fill_(1.)

net.train()   # 训练时缩放因子为2，除以1-d_prob=0.5, 相当于乘以 2
y = net(x)
print('output in training mode', y)

net.eval()
y = net(x)
print('output in eval mode', y)
